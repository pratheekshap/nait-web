




        # Connect to the database
        engine = create_engine(db_url)

        # Fetch data using SQL query
        df = pd.read_sql(kwargs['sql_query'], engine)

        # Close the database connection
        engine.dispose()

        return df

    except Exception as e:
        st.error(f"An error occurred while fetching data from the database: {e}")
        return None

# Function to fetch data from MS SQL Server database
def fetch_data_from_mssql(server_name, database_name, authentication, username, password, sql_query):
    try:
        if authentication == "Windows Authentication":
            db_url = f"mssql+pyodbc://{server_name}/{database_name}?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server"
        elif authentication == "SQL Server Authentication":
            db_url = f"mssql+pyodbc://{username}:{password}@{server_name}/{database_name}?driver=ODBC+Driver+17+for+SQL+Server"

        # Connect to the MS SQL Server database
        engine = create_engine(db_url)

        # Fetch data using SQL query
        df = pd.read_sql(sql_query, engine)

        

        return df
    except Exception as e:
        st.error(f"An error occurred while fetching data from the database: {e}")
        return None



# Streamlit UI Setup
st.title("Welcome To NAIT")
st.write('Network Analysis and Intelligence Tool')

# Data Upload Options
data_source = st.radio("Select data source:", ("File Upload", "SQL Database"))



df = None
data_fetched = False

# Inside the "File Upload" section
if data_source == "File Upload":
    with st.expander("File Upload"):
        data = st.file_uploader("Please import your dataset", type=["csv", "txt", "xlsx", "xls"])
        if data is not None:
            file_extension = data.name.split(".")[-1]
            if file_extension.lower() == "csv":
                df = pd.read_csv(data)
            elif file_extension.lower() == "txt":
                df = pd.read_csv(data, delimiter="\t")  # Assuming tab-separated text file, adjust delimiter as needed
            elif file_extension.lower() in ["xlsx", "xls"]:
                df = pd.read_excel(data)

            data_fetched = True
            st.success("Data loaded successfully.")

            # Specify the custom datetime format if needed
            date_format = "%m/%d/%Y %I:%M:%S %p"
            if "Date Time" in df.columns:
                # Clean the datetime strings
                df["Date Time"] = df["Date Time"].str.split(" - ").str[0]
                # Convert to datetime
                df["Date Time"] = pd.to_datetime(df["Date Time"], format=date_format, errors='coerce')
                # Check for unconverted data
                unconverted_data = df.loc[df["Date Time"].isna(), "Date Time"]
                if not unconverted_data.empty:
                    st.warning(f"Unconverted data found: {unconverted_data}")
                    st.warning("Please verify the datetime format and data consistency.")
    
                # Store the fetched data in session state
                st.session_state.data = df
            else:
                st.error("Unsupported file format. Please upload a CSV, TXT, XLSX, or XLS file.")

elif data_source == "SQL Database":
    with st.expander("SQL Database"):
        db_type = st.selectbox("Database Type", ["mysql", "mssql","PostgreSQL"])
        if db_type == "mysql":
            username = st.text_input("Username")
            password = st.text_input("Password", type="password")
            host = st.text_input("Host")
            port = st.text_input("Port")
            database_name = st.text_input("Database Name")

            sql_query = st.text_area("SQL Query", "SELECT * FROM your_table WHERE your_conditions")

            if st.button("Fetch Data"):
                try:
                    # Construct the MySQL database connection URL
                    db_url = f"mysql+mysqlconnector://{username}:{password}@{host}:{port}/{database_name}"

                    # Connect to the MySQL database
                    engine = create_engine(db_url)

                    # Fetch data using SQL query
                    df = pd.read_sql(sql_query, engine)

                    # Preprocess datetime strings: remove extra information
                    df["Date Time"] = df["Date Time"].str.split(" - ").str[0]

                    # Specify the custom datetime format if needed
                    date_format = "%m/%d/%Y %I:%M:%S %p"
                    if "Date Time" in df.columns:
                        df["Date Time"] = pd.to_datetime(df["Date Time"], format=date_format)
                        
                    # Store fetched data in session state
                    st.session_state.data = df

                    data_fetched = True
                    # Provide feedback if data fetching was successful
                    st.success("Data fetched successfully from the database.")

                    # Close the database connection
                    engine.dispose()

                except Exception as e:
                    st.error(f"An error occurred while fetching data from the database: {e}")

        elif db_type == "mssql":
            server_name = st.text_input("Server Name")
            database_name = st.text_input("Database Name")
            authentication = st.selectbox("Authentication", ["Windows Authentication", "SQL Server Authentication"])
            
            if authentication == "SQL Server Authentication":
                username = st.text_input("Username")
                password = st.text_input("Password", type="password")
# Allow users to input their custom SQL query
            sql_query = st.text_area("SQL Query")

            if st.button("Fetch Data"):
                try:
                    # Construct the MS SQL Server database connection URL
                    if authentication == "Windows Authentication":
                        db_url = f"mssql+pyodbc://{server_name}/{database_name}?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server"
                    elif authentication == "SQL Server Authentication":
                        db_url = f"mssql+pyodbc://{username}:{password}@{server_name}/{database_name}?driver=ODBC+Driver+17+for+SQL+Server"

                    # Connect to the MS SQL Server database
                    engine = create_engine(db_url)

                    # Example SQL query
                    #sql_query = "SELECT * FROM FROM [BANDWIDTH].[dbo].[Bandwidth Test Data for 1 year]"

                    # Fetch data using SQL query
                    df = pd.read_sql(sql_query, engine)

                    # Specify the custom datetime format if needed
                    date_format = "%m/%d/%Y %I:%M:%S %p"
                    if "Date Time" in df.columns:
                        # Clean the datetime strings
                        df["Date Time"] = df["Date Time"].str.split(" - ").str[0]
                        # Convert to datetime
                        df["Date Time"] = pd.to_datetime(df["Date Time"], format=date_format, errors='coerce')
                        # Check for unconverted data
                        unconverted_data = df.loc[df["Date Time"].isna(), "Date Time"]
                        if not unconverted_data.empty:
                            st.warning(f"Unconverted data found: {unconverted_data}")
                            st.warning("Please verify the datetime format and data consistency.")
                    
                    # Store fetched data in session state
                    st.session_state.data = df

                    data_fetched = True
                    # Provide feedback if data fetching was successful
                    st.success("Data fetched successfully from the database.")

                    # Close the database connection
                    engine.dispose()

                except Exception as e:
                    st.error(f"An error occurred while fetching data from the database: {e}")

        elif db_type == "PostgreSQL":
                postgres_host = st.text_input("Host")
                postgres_port = st.text_input("Port")
                postgres_database_name = st.text_input("Database Name")
                postgres_user = st.text_input("Username")
                postgres_password = st.text_input("Password", type="password")
                postgres_sql_query = st.text_area("SQL Query")

        if st.button("Fetch Data"):
                try:
            # Construct the PostgreSQL database connection URL
                        postgres_db_url = f"postgresql://{postgres_user}:{postgres_password}@{postgres_host}:{postgres_port}/{postgres_database_name}"

            # Connect to the PostgreSQL database
                        postgres_engine = create_engine(postgres_db_url)

            # Fetch data using SQL query
                        df = pd.read_sql(postgres_sql_query, postgres_engine)

            # Specify the custom datetime format if needed
                        date_format = "%d.%m.%Y %I:%M:%S %p"
                        if "Date Time" in df.columns:
                            df["Date Time"] = df["Date Time"].str.split(" - ").str[0]
                            df["Date Time"] = pd.to_datetime(df["Date Time"], format=date_format, errors='coerce')
                            unconverted_data = df.loc[df["Date Time"].isna(), "Date Time"]
                            if not unconverted_data.empty:
                                st.warning(f"Unconverted data found: {unconverted_data}")
                                st.warning("Please verify the datetime format and data consistency.")

            # Provide feedback if data fetching was successful
                        st.success("Data fetched successfully from the PostgreSQL database.")

            # Close the database connection
                        postgres_engine.dispose()

                        if 'df' in locals() and not df.empty:
                            st.write(df)

                except Exception as e:
                    st.error(f"An error occurred while fetching data from the PostgreSQL database: {e}")  

# Proceed with further steps after fetching data
if data_fetched and st.session_state.data is not None:
    df = st.session_state.data

    # Display the data
    st.write(df)
    

    if df.isnull().values.any():
            missing_percentage = df.isnull().mean() * 100
            st.warning("The imported dataset contains null values.")
            warning_message = "Percentage of missing values by column:\n"
            for column, percentage in missing_percentage.items():
                warning_message += f"- {column}: {percentage:.2f}%\n"
            st.warning(warning_message)

            options = ['Fill 0', 'Mean', 'Mode', 'Forward Fill', 'Backward Fill','Linear Interpolation','Remove null']
            selected_option = st.radio("Which method would you preffer to treat them", options)

            if selected_option == 'Fill 0':
                df1 = df.fillna(0)
            elif selected_option== 'Mode':
                df1 = df.fillna(df.mode().iloc[0])
            elif selected_option == 'Mean':
                df_numeric = df.apply(pd.to_numeric, errors='coerce')
                means = df_numeric.mean()
                df1 = df_numeric.fillna(means)
            elif selected_option == 'Forward Fill':
                df1 = df.ffill()
            elif selected_option == 'Backward Fill':
                df1 = df.bfill()
            elif selected_option == 'Linear Interpolation':
                df1 = df.interpolate(method='linear', limit_direction='both')
            elif selected_option == 'Remove null':
                df1 = df.dropna()
            else:
                df1=df

    else:
        df1 = df.copy()  # If no null values, make a copy of df

   

    startDate = pd.to_datetime(df1["Date Time"]).min()
    endDate = pd.to_datetime(df1["Date Time"]).max()

        # Create columns for user inputs
    col1, col2, col3 = st.columns(3)

    with col1:
        date1 = st.date_input("Start Date", startDate)

    with col2:
        date2 = st.date_input("End Date", endDate)

    with col3:
        start_time = st.time_input("Start Time", datetime.strptime("00:00:00", "%H:%M:%S").time())
        end_time = st.time_input("End Time", datetime.strptime("23:59:59", "%H:%M:%S").time())

        # Filter the DataFrame based on selected date and time range
    start_datetime = datetime.combine(date1, start_time)
    end_datetime = datetime.combine(date2, end_time)
    
    if start_datetime < df1["Date Time"].min() or end_datetime > df1["Date Time"].max():
        st.warning("Selected date range is outside the available data. Please choose a valid date range.")
    else:
        df1 = df1[(df1["Date Time"] >= start_datetime) & (df1["Date Time"] <= end_datetime)].copy()
    
    c = st.multiselect(
         "Please select the required columns to be analysed",
         options=df1.columns
    )

    df2 = df1[c]
    st.write(df2)

    
    
 

    #percentile
    percentile_values = []  # List to store percentile values
    n = st.number_input('Enter the Percentile')
    for column_name in c:
        if np.issubdtype(df2[column_name].dtype, np.number):
            # Convert 'Date Time' column to datetime type
             df2['Date Time'] = pd.to_datetime(df2['Date Time'])
             df2['Date Time'] = df2['Date Time'].dt.date
             daily_percentile_traffic = df2.groupby('Date Time')[c[1]].quantile(n)
             percentile_val = np.percentile(df2[column_name].values, n)
             percentile_values.append(daily_percentile_traffic)

    # Display only the percentile values without any labels
    for value in percentile_values:
        st.write("For the", n,"Percentile.The bandwidth value is",value)


    # Find the peak value for the "Traffic Total (speed)(RAW)" column
    peak_traffic = df2[c[1]].max()
    daily_peak_traffic = df2.groupby('Date Time')[c[1]].max()
    peak_datetime = df2.loc[df2.groupby('Date Time')[c[1]].idxmax(), c]

    # Find the corresponding "Date Time" value for the peak traffic
    st.write("The Peak value is  ", peak_datetime)

    #average 
    average_bandwidth = df2[c[1]].mean() 
    daily_average_traffic = df2.groupby('Date Time')[c[1]].mean()
    st.write("The average value is  ", daily_average_traffic)

    percentile_values = []  # List to store percentile values
    percentile_trendlines = []  # List to store trendlines for percentile values

    for column_name in c:
        if np.issubdtype(df2[column_name].dtype, np.number):
            # Convert 'Date Time' column to datetime type
            df2['Date Time'] = pd.to_datetime(df2['Date Time'])
            df2['Date Time'] = df2['Date Time'].dt.date
            daily_percentile_traffic = df2.groupby('Date Time')[column_name].quantile(n)
            percentile_val = np.percentile(df2[column_name].values, n)
            percentile_values.append(daily_percentile_traffic)

            # Calculate linear trendlines for the percentile data
            x = np.arange(len(daily_percentile_traffic)).reshape(-1, 1)
            y_percentile = daily_percentile_traffic.values.reshape(-1, 1)
            trendline_percentile = LinearRegression().fit(x, y_percentile)
            percentile_trendlines.append(trendline_percentile)

    # Find the peak value for the specified column
    peak_traffic = df2[c[1]].max()
    daily_peak_traffic = df2.groupby('Date Time')[c[1]].max()
    peak_datetime = df2.loc[df2.groupby('Date Time')[c[1]].idxmax(), c[0]]

    # Find the average value for the specified column
    average_bandwidth = df2[c[1]].mean()
    daily_average_traffic = df2.groupby('Date Time')[c[1]].mean()

    # Calculate linear trendlines for peak and average values
    x_percentile = np.arange(len(daily_percentile_traffic)).reshape(-1, 1)
    y_percentile = daily_percentile_traffic.values.reshape(-1, 1)
    trendline_percentile = LinearRegression().fit(x_percentile, y_percentile)
    slope_percentile = trendline_percentile.coef_[0][0]
    intercept_percentile= trendline_percentile.intercept_[0]

    x_peak = np.arange(len(daily_peak_traffic)).reshape(-1, 1)
    y_peak = daily_peak_traffic.values.reshape(-1, 1)
    trendline_peak = LinearRegression().fit(x_peak, y_peak)
    slope_peak = trendline_peak.coef_[0][0]
    intercept_peak = trendline_peak.intercept_[0]

    x_average = np.arange(len(daily_average_traffic)).reshape(-1, 1)
    y_average = daily_average_traffic.values.reshape(-1, 1)
    trendline_average = LinearRegression().fit(x_average, y_average)
    slope_average = trendline_average.coef_[0][0]
    intercept_average = trendline_average.intercept_[0]

    # Create a Plotly figure for all three lines and the trendlines
    fig = go.Figure()

    # Add the percentile line and its trendline (in blue)
    for i, (value, trendline) in enumerate(zip(percentile_values, percentile_trendlines)):
        fig.add_trace(go.Scatter(x=value.index, y=value, mode='lines', name=f'{n}th Percentile - {c[i]}', line=dict(color='aqua')))
        x_trend = np.arange(len(value))
        y_trend_percentile = trendline.predict(x_trend.reshape(-1, 1))
        fig.add_trace(go.Scatter(x=value.index, y=y_trend_percentile.flatten(), mode='lines', name=f'{n}th Percentile Trendline - {c[i]}', line=dict(dash='dash', color='aqua')))

    # Add the peak line and its trendline (in red)
    fig.add_trace(go.Scatter(x=daily_peak_traffic.index, y=daily_peak_traffic, mode='lines', name=f'Peak - {c[1]}', line=dict(color='red')))
    y_trend_peak = trendline_peak.predict(x_peak)
    fig.add_trace(go.Scatter(x=daily_peak_traffic.index, y=y_trend_peak.flatten(), mode='lines', name=f'Peak Trendline (Slope: {slope_peak:.2f})', line=dict(dash='dash', color='red')))

    # Add the average line and its trendline (in green)
    fig.add_trace(go.Scatter(x=daily_average_traffic.index, y=daily_average_traffic, mode='lines', name=f'Average - {c[1]}', line=dict(color='green')))
    y_trend_average = trendline_average.predict(x_average)
    fig.add_trace(go.Scatter(x=daily_average_traffic.index, y=y_trend_average.flatten(), mode='lines', name=f'Average Trendline (Slope: {slope_average:.2f})', line=dict(dash='dash', color='green')))

    # Customize the layout
    fig.update_layout(
        title='Bandwidth Trends',
        xaxis_title='Time(Date)',
        yaxis_title='Sensor value',
        xaxis=dict(type='category'),  # Set x-axis type to 'category' for dates
        template='plotly_dark'  # Use a dark theme
    )
    # Display the interactive graph using Plotly
    st.plotly_chart(fig)  
    
